package com.test.log4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoggingDemo {
    private static final Logger log = LogManager.getLogger(LoggingDemo.class.getName());

    public static void main(String[] args) {
        //while running it, only prints error and fatal ,
        // create configs and add Log4j2.xml
        log.trace("trace testing logs");
        log.debug("debug testing logs");
        log.info("info testing logs");
        log.error("error testing logs");
        log.fatal("fatal testing logs");
    }
}
